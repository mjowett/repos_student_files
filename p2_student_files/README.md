Required: First, delete these notes, and include your own, before pushing
to your remote repo. This file *must* use Markdown syntax, and provide project
documentation--otherwise, points *will* be deducted.

# Project2 Notes:
- Bootstrap Carousel: place on home page
- http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-carousel.php
- http://www.w3schools.com/bootstrap/bootstrap_carousel.asp

- RSS Feeds: Background info
 (your submission *must* use another RSS feed)
 What is RSS? http://www.whatisrss.com/
 RSS Specification: http://www.rss-specifications.com/rss-specifications.htm
 Anatomy of an RSS feed: http://www.webreference.com/authoring/languages/xml/rss/feeds/index.html

- RSS Feeds: Examples
- https://www.developphp.com/video/PHP/simpleXML-Tutorial-Learn-to-Parse-XML-Files-and-RSS-Feeds
- http://www.w3schools.com/rss/

- Mashups: Background info and Examples
- https://developer.salesforce.com/page/Mashups:_The_What_and_Why
- http://www.programmableweb.com/ > "Latest Mashups" (rt-side search)
