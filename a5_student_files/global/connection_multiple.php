<?php
//1. comment both of these lines, save the file, then upload to your global subdirectory (that is in each assignment directory) on your remote Web host. (The reason for do so is that each "client" (here, assignment) could conceivably have its own discrete set of connection values.)
//2. *after* the above step, uncomment the "local" $IP variable below, and save this file locally, so that it will work locally

//$IP="local";
//$IP="cci";

$options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

//contact your Web host for DB connection documentation
//example:
if ($IP=="local")
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=yourdbname';
	$username = 'yourusername';
	$password = 'yourpassword';
}
else if ($IP=="cci")
{
	$dsn = 'mysql:host=mysql-jowett.cci.fsu.edu;port=3306;dbname=yourdbname';
	$username = 'yourusername';
	$password = 'yourpassword';
}

else
{
	$dsn = 'mysql:host=localhost;port=3306;dbname=yourdbname';
	$username = 'yourusername';
	$password = 'yourpassword';
}

try 
{
  //instantiate new PDO connection
  $db = new PDO($dsn, $username, $password, $options);
	  //echo "Connected successfully using pdo extension!<br /><br />";
} 
catch (PDOException $e) 
{
	//only use for testing, to avoid providing security exploits
	//after testing, create custom error message
  //echo $e->getMessage();  //display error on this page
  $error = $e->getMessage(); 
  include('error.php'); //display in custom error page
  exit();
}
?>
