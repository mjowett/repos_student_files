Required: First, delete these notes, and include your own, before pushing
to your remote repo. This file *must* use Markdown syntax, and provide project
documentation--otherwise, points *will* be deducted.
Also, A5 should include add_petstore.php, edit_petstore.php, and
delete_process.php. 
