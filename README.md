Required: Contains template files and directories that can be modified
accordingly.
This README.md file should be placed at the root of your repos directory
that contains the sub-directories of your assignments and projects
(e.g., a1, a2, etc.), including each of their individual
sub-directories.
*Also, please create a link in the README.md file to where your
assignment is located on your remote Web host.*
First, delete these notes, and include your own, before pushing
to your remote repo. This file *must* use Markdown syntax, and provide project
documentation--otherwise, points *will* be deducted.
